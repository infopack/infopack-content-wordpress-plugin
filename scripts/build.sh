#/bin/sh

if [ $# -eq 0 ];
then
  echo "$0: Missing arguments"
  exit 1
fi

echo "Building package with version tag: $1"

rm -rf ./dist
mkdir ./dist

rm -rf ./build
mkdir ./build


cp -r infopack-content-plugin ./build/infopack-content-plugin

# update version in style.css
sed -i "s/^Version:.*/Version:        $1/" build/infopack-content-plugin/index.php

FILE_VERSION="$(echo $1 | sed 's/^.*: //' | sed 's/\./_/gI')"

cd build
zip -r "../dist/infopack-content-plugin-$FILE_VERSION.zip" infopack-content-plugin/
cd ..

# clean up
rm -rf ./build
