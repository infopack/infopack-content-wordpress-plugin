<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function infopack_add_bot_user() {
  if(!get_user_by('login', 'infopack_bot')) {
    wp_insert_user(array(
      'user_login' => 'infopack_bot',
      'user_pass' => infopack_utils_generate_random_string(24),
      'display_name' => 'Infopack Bot'
    ));
  }
}
