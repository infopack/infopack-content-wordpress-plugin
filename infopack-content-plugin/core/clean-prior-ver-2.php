<?php

/**
 * These function cleans up old functions from earlier versions. Can be removed in future release
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function infopack_delete_redirect_table_hook() {
    global $wpdb;
    
    $table_name = $wpdb->prefix . 'infopack_redirect_table';
    
    // Kontrollera om tabellen finns innan den raderas
    $table_exists = $wpdb->get_var("SHOW TABLES LIKE '$table_name'");
    
    if ($table_exists === $table_name) {
        $sql = "DROP TABLE $table_name";
        $wpdb->query($sql);
    }
    
}

function infopack_delete_infopack_current_version_option() {
    if (get_option('infopack_current_version_options') !== false) {
        delete_option('infopack_current_version_options');
    }
}
