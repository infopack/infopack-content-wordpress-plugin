<?php

function infopack_utils_generate_random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
}

function infopack_utils_decode_irn($irn_string) {
    preg_match('/(.*):(.*):(.*):(.*)/', $irn_string, $matches);

    if(count($matches) != 5) {
        return false;
    }

    return array(
        "namespace" => $matches[1],
        "package_name" => $matches[2],
        "version" => $matches[3],
        "file_path" => $matches[4]
    );
}

function infopack_utils_attributes_to_url($atts) {
    if( array_key_exists('irn', $atts) ) {
        return $atts['irn'];
    }

    if(!array_key_exists('namespace', $atts)) return false;
    if(!array_key_exists('package_name', $atts)) return false;
    if(!array_key_exists('version', $atts)) return false;
    if(!array_key_exists('file_path', $atts)) return false;

    return $atts['namespace'] . '/' . $atts['package_name'] . '/' . $atts['version'] . '/' . $atts['file_path'];
}

/**
 * Returns atts from shortcode apps. Resolves irn property (if provided)
 */
function infopack_utils_resolve_shortcode_atts($shortcode_atts) {
    /**
     * This will be normalized array containing namespace, package_name, version and path.
     */
    $atts = array();

    if(!is_array($shortcode_atts)) {
      return [0, "No attributes provided"];
    }
    /**
     * Experimental, see https://gitlab.com/infopack/infopack-framework/-/issues/10 for more info
     */
    if(array_key_exists('irn', $shortcode_atts)) {

      $atts = infopack_utils_decode_irn($shortcode_atts['irn']);

    } else {

      if(!array_key_exists('namespace', $shortcode_atts)) {
        return [false, 'You must provide a namespace attribute. Example: [infopack-content-plugin namespace=...]'];
      }
      $atts['namespace'] = $shortcode_atts['namespace'];

      if(!array_key_exists('package_name', $shortcode_atts)) {
        return [false, 'You must provide a package name attribute. Example: [infopack-content-plugin package_name=...]'];
      }
      $atts['package_name'] = $shortcode_atts['package_name'];

      if(!array_key_exists('version', $shortcode_atts)) {
        return [false, 'You must provide a version attribute. Example: [infopack-content-plugin version=...]'];
      }
      $atts['version'] = $shortcode_atts['version'];

      if(!array_key_exists('file_path', $shortcode_atts)) {
        return [false, 'You must provide a file path attribute. Example: [infopack-content-plugin file_path=...]'];
      }
      $atts['file_path'] = $shortcode_atts["file_path"];
    }

    if(array_key_exists('major_only', $shortcode_atts)) {
        $atts['major_only'] = $shortcode_atts['major_only'];
    }
        
    return [true, $atts];
}

function infopack_utils_generate_url($atts) {
    return 'https://storage.googleapis.com/storage.infopack.io/' . $atts['namespace'] . '/' . $atts['package_name'] . '/' . $atts['version'] . '/' . $atts['file_path'];
}

function infopack_path_join() {
    $working_dir = "";
    foreach(func_get_args() as $p) {
        if($p === null || $p === '') continue;
        elseif($p[0] === '/') $working_dir = $p;
        elseif($working_dir === "") $working_dir = $p;
        else $working_dir .= "/$p";
    }
   // $working_dir = preg_replace('~/{2,}~','/', $working_dir);
    if($working_dir === '/') return '/';
    $out = [];
    foreach(explode('/',rtrim($working_dir,'/')) as $p) {
        if($p === '.') continue;
        if($p === '..') array_pop($out);
        else $out[] = $p;
    }
    return implode('/',$out);
}

function infopack_util_extract_path_with_full_paths($path) {
    // Dela upp sökvägen i segment
    $segments = explode('/', $path);

    // Om det bara finns ett segment, returnera tom array
    if (count($segments) <= 1) {
        return [];
    }

    // Ta bort sista segmentet
    array_pop($segments);

    $unique_segments = [];
    $full_path = "";
    $previous_full_path = "";

    foreach ($segments as $segment) {
        if (!in_array($segment, array_column($unique_segments, 'segment'))) { // Kontrollera att segmentet är unikt
            $full_path .= ($full_path === "") ? $segment : "/$segment"; // Bygg fullständig sökväg
            $unique_segments[] = [
                'segment' => $segment,
                'full_path' => $full_path,
                'previous_full_path' => $previous_full_path // Lagra föregående sökväg
            ];
            $previous_full_path = $full_path; // Uppdatera för nästa iteration
        }
    }

    return $unique_segments;
}

/**
 * generates a collection identifier string
 */
function infopack_util_extract_collection_identifier($input) {
    $parts = explode('/', $input);

    $first_three = array_slice($parts, 0, 3);

    return implode('-', $first_three);
}

function infopack_util_convert_collection_path($input) {
    $parts = explode('/', $input);

    $first_three = implode('-', array_slice($parts, 0, 3));

    $remaining = implode('/', array_slice($parts, 3));

    return $remaining ? $first_three . '/' . $remaining : $first_three;
}


function infopack_build_path($parts) {
    $filtered_parts = array_filter($parts, function($part) {
        return $part !== null && $part !== '';
    });
    return implode('/', $filtered_parts);
}

/**
 * Takes an infopack collection content item and preps it with sync identifiers
 */
function infopack_utils_update_file_path($item) {

	$item->collection_identifier = infopack_util_extract_collection_identifier($item->collectionPath);
	$item->sync_identifier = infopack_build_path(array(infopack_util_convert_collection_path($item->collectionPath), infopack_util_convert_collection_path($item->packagePath), $item->filePath));

    // Dela upp path i mappar och filnamn
    $parts = explode("/", $item->sync_identifier);
    $fileName = array_pop($parts); // Ta bort filen från arrayen

    // Ta bort ".partial.html" från filnamnet
    $cleanFileName = str_replace(".partial.html", "", $fileName);

    // Om den sista mappen matchar filnamnet, ta bort filen och behåll bara mappens path
    if (end($parts) === $cleanFileName) {
        $item->sync_identifier = implode("/", $parts);
    }

    return $item;
}

function infopack_utils_get_content_from_irn($irn_str) {
    $irn = infopack_utils_decode_irn($irn_str);
    $content_url = infopack_utils_generate_url( $irn );

    $content_response = wp_remote_get( $content_url );
    $content = wp_remote_retrieve_body( $content_response );

    return infopack_content_renderer( $irn, $content );
}

function infopack_utils_deactivate_all_infopack_collections() {
    global $wpdb;

    // Hämta alla inlägg av typen 'infopack_collection'
    $posts = get_posts(array(
        'post_type'      => 'infopack_collection',
        'posts_per_page' => -1, // Hämta alla inlägg
        'fields'         => 'ids' // Vi behöver bara ID:n
    ));

    // Kolla om det finns några inlägg att uppdatera
    if (!empty($posts)) {
        foreach ($posts as $post_id) {
            update_post_meta($post_id, 'active', false);
        }
        return count($posts) . ' inlägg uppdaterades!';
    } else {
        return 'Inga inlägg hittades att uppdatera.';
    }
}
