<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// show infopack content in standard query
add_action( 'pre_get_posts', 'infopack_add_infopack_content_to_query' );

function infopack_add_infopack_content_to_query($query) {
  if ( $query->is_home() && $query->is_main_query() ) {
    $query->set('post_type', array( 'post', 'infopack_content' ) );
  }
}

function infopack_setup_custom_post_type() {

  register_post_type('infopack_collection', array(
    'label' => 'Infopack collection',
    'public' => false,
    'menu_icon' => 'dashicons-tag',
    'has_archive' => true,
    'show_in_rest' => false,
    'show_ui' => true,
    'supports' => array(
      'title',
      'editor',
      'custom-fields',
    )
  ));

  register_post_type('infopack_content', array(
    'label' => 'Infopack content',
    'public' => true,
    'menu_icon' => 'dashicons-tag',
    'has_archive' => true,
    'hierarchical' => true,
    'show_in_rest' => true,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'capability_type' => 'page',
    'exclude_from_search' => false,
    'rewrite' => true,
    'show_in_menu' => true,
    'show_ui' => true,
    'supports' => array(
      'title',
      'editor',
      'custom-fields',
      'page-attributes'
    )
  ));
}
