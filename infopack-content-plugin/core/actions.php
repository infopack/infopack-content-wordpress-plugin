<?php

// If this file is called directly, abort.
if (! defined('WPINC')) {
	die;
}

// Sync an infopack content
add_action('admin_post_infopack_admin_sync', 'infopack_admin_sync');

function infopack_admin_sync()
{
	$post_id = $_REQUEST['post_id'];

    infopack_update_infopack_content($post_id);

    wp_redirect(admin_url('edit.php?post_type=infopack_content&status=content_synced'));
    exit;
}

add_action('admin_post_infopack_activate_collection', 'infopack_activate_collection');
/**
 * Starts the sync of content from a collection
 */
function infopack_activate_collection() {
    if (!isset($_REQUEST['post_id'])) {
        wp_die('Fel: post_id saknas i förfrågan.', 'Fel', array('response' => 400));
    }

    $post_id = intval($_REQUEST['post_id']);

    // Säkerställ att post_id är giltigt och att inlägget existerar
    if ($post_id <= 0 || get_post_type($post_id) !== 'infopack_collection') {
        wp_die('Fel: Ogiltigt post_id eller inlägget existerar inte.', 'Fel', array('response' => 400));
    }

    infopack_utils_deactivate_all_infopack_collections();

    update_post_meta($post_id, 'active', '1');

    wp_redirect(admin_url('edit.php?post_type=infopack_collection&status=collection_activated'));
    exit;
}

add_action('admin_post_sync_collection_button', 'infopack_action_load_content_from_collection');
/**
 * Starts the sync of content from a collection
 */
function infopack_action_load_content_from_collection() {
    if (isset($_REQUEST['post_id'])) {

        // Verifiera Nonce
        // if (!isset($_POST['my_plugin_nonce']) || !wp_verify_nonce($_POST['my_plugin_nonce'], 'my_plugin_action')) {
        //     wp_die('Säkerhetskontroll misslyckades.');
        // }

        $post_id = intval($_REQUEST['post_id']); // Hämta rätt Collection ID
        $raw_content = get_post_field('post_content', $post_id); // Hämta huvudcontent

        if (empty($raw_content)) {
            wp_die('Inga data hittades i huvudcontent.');
        }

        $arr = json_decode($raw_content);

        if (!isset($arr) || empty($arr)) {
            wp_die('Ingen innehållsdata att synka.');
        }

        // Konvertera och uppdatera paths
        $edited_content = array_map("infopack_utils_update_file_path", $arr->content);

        foreach ($edited_content as $content) {
            if (!isset($content->irn)) {
                continue;
            }
            infopack_create_infopack_content(
                $content->collection_identifier,
                $content->sync_identifier,
                $content->irn,
                $content->meta
            );
        }

        // Visa ett meddelande om framgång
        add_action('admin_notices', function () {
            echo '<div class="notice notice-success"><p>Collection har synkats!</p></div>';
        });

        wp_redirect(admin_url('edit.php?post_type=infopack_collection')); // Omdirigera tillbaka till listan
        exit;
    } else {
        wp_die('Fel: Formuläret skickade inte nödvändiga värden.');
    }
}

/**
 * 
 */
function infopack_create_infopack_content($collection_identifier, $sync_identifier, $irn_str, $meta) {
    $segments = infopack_util_extract_path_with_full_paths($sync_identifier);
    $parent_id;

    foreach ($segments as $segment) {
        $posts = get_posts(array(
            'numberposts'   => 1,
            'post_type'     => 'infopack_content',
            'meta_key'      => 'sync_identifier',
            'meta_value'    => $segment['full_path']
        ));
    
        if(count($posts) == 0) {
            // we need to create it
            $embryo = array(
                'post_type' => 'infopack_content',
                'post_author' => get_user_by('login', 'infopack_bot')->ID,
                'post_title' => ucfirst(str_replace('-', ' ', $segment['segment'])),
                'post_excerpt' => 'Navigeringssida',
                'post_content' => '<!-- wp:shortcode -->[infopack-subpages]<!-- /wp:shortcode -->',
                'post_name' => $segment['segment'],
                'post_status' => 'publish',
                'meta_input' => array(
                    'collection_identifier' => $collection_identifier,
                    'sync_identifier' => $segment['full_path'],
                    'last_synced' => date("Y-m-d H:i:s")
                )
            );

            // should it have a parent?
            if($segment['previous_full_path'] != '') {
                // look it up
                $posts = get_posts(array(
                    'numberposts'   => 1,
                    'post_type'     => 'infopack_content',
                    'meta_key'      => 'sync_identifier',
                    'meta_value'    => $segment['previous_full_path']
                ));
                
                if(count($posts) > 0) {
                    // yes!
                    $embryo['post_parent'] = $posts[0]->ID;
                } else {
                    error_log('PARENT MISSING');
                }
            }

            $parent_id = wp_insert_post($embryo);

        } else {
            // store it
            $parent_id = $posts[0]->ID;
        }
    }

    $sync_identifier_arr = explode('/', $sync_identifier);
    $main_name = array_pop($sync_identifier_arr);

    $content = infopack_utils_get_content_from_irn($irn_str);

    $posts = get_posts(array(
        'numberposts'   => 1,
        'post_type'     => 'infopack_content',
        'meta_key'      => 'sync_identifier',
        'meta_value'    => $sync_identifier
    ));

    $id;

    if(count($posts) > 0) {
        $id = infopack_update_infopack_content($posts[0]->ID);
    } else {
        $id = wp_insert_post(array(
            'post_type' => 'infopack_content',
            'post_author' => get_user_by('login', 'infopack_bot')->ID,
            'post_title' => $meta->title,
            'post_excerpt' => !empty($meta->description) ? $meta->description: 'Infopack innehåll',
            'post_content' => $content,
            'post_name' => $main_name,
            'post_status' => 'publish',
            'post_parent' => $parent_id,
            'meta_input' => array(
                'collection_identifier' => $collection_identifier,
                'sync_identifier' => $sync_identifier,
                'irn' => $irn_str,
                'last_synced' => date("Y-m-d H:i:s"),
                'meta' => json_encode($meta, JSON_UNESCAPED_UNICODE)
            )
        ));
    }
    return $id;
}

add_action('admin_post_process_infopack_collection', 'process_infopack_collection');

function process_infopack_collection() {
    if (!current_user_can('manage_options')) {
        wp_die(__('Du har inte behörighet att utföra denna åtgärd.', 'textdomain'));
    }

    if (!isset($_POST['infopack_collection_nonce']) || !wp_verify_nonce($_POST['infopack_collection_nonce'], 'infopack_collection_action')) {
        wp_die(__('Säkerhetskontrollen misslyckades. Försök igen.', 'textdomain'));
    }

    $json_data = wp_unslash($_POST['infopack_collection_string']);
  
    $headers = array(
        'Content-Type' => 'application/json'
    );
    $response = wp_remote_post(getenv('COLLECTION_API_URL') . '/collection/expanded?returnContent=false', array('headers' => $headers, 'body' => $json_data));

    if (!is_wp_error($response) && wp_remote_retrieve_response_code($response) == 200) {
        $api_body = wp_remote_retrieve_body($response);
        $api_data = json_decode($api_body, true);

        infopack_utils_deactivate_all_infopack_collections();
  
        if (isset($api_data['title'])) {
            $post_id = wp_insert_post(array(
                'post_type'   => 'infopack_collection',
                'post_status' => 'publish',
                'post_title'  => sanitize_text_field($api_data['title']),
                'post_content'=> wp_kses_post($api_body),
                'meta_input' => array(
                    'active' => true,
                    'collection_identifier' => $api_data['name'] . '-' . $api_data['version'] . '-' . $api_data['content'][0]['fetchedDate'],
                    'version' => $api_data['version']
              )
            ));
  
            if ($post_id) {
                update_post_meta($post_id, '_custom_json_data', $json_data);

                wp_redirect(admin_url('edit.php?post_type=infopack_collection&status=success'));
                exit;
            }
        } else {
            wp_redirect(admin_url('edit.php?post_type=infopack_collection&status=api_error'));
            exit;
        }
    } else {
        wp_redirect(admin_url('edit.php?post_type=infopack_collection&status=api_fail'));
        exit;
    }
}

function infopack_update_infopack_content($id) {
    $irn_str = get_post_meta($id, 'irn', true);

    if(!$irn_str) {
        // parent pages does not have irns...
        return false;
    }

    $content = infopack_utils_get_content_from_irn($irn_str);

    $id = wp_update_post( array(
        'ID' => $id,
        'post_content' => $content,
        'meta_input' => array(
            'last_synced' => date("Y-m-d H:i:s")
        )
    ) );

    return $id;
}

add_action('admin_notices', function () {
    if (isset($_GET['status'])) {
        if ($_GET['status'] == 'success') {
            echo '<div class="notice notice-success is-dismissible"><p>Infopacket har skapats!</a></p></div>';
        } elseif ($_GET['status'] == 'api_error') {
            echo '<div class="notice notice-error is-dismissible"><p>Fel: API-svaret innehåller ingen titel eller innehåll.</p></div>';
        } elseif ($_GET['status'] == 'api_fail') {
            echo '<div class="notice notice-error is-dismissible"><p>API-förfrågan misslyckades.</p></div>';
        } elseif ($_GET['status'] == 'content_synced') {
            echo '<div class="notice notice-success is-dismissible"><p>Innehållet har synkats.</p></div>';
        } elseif ($_GET['status'] == 'collection_activated') {
            echo '<div class="notice notice-success is-dismissible"><p>Aktiv collection har uppdaterats!</p></div>';
        }
    }
});
