<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// load sub components
require_once plugin_dir_path( __FILE__ ) . 'clean-prior-ver-2.php';
require_once plugin_dir_path( __FILE__ ) . 'bot_user.php';
require_once plugin_dir_path( __FILE__ ) . 'custom_post_type.php';
require_once plugin_dir_path( __FILE__ ) . 'utils.php';
require_once plugin_dir_path( __FILE__ ) . 'renderers.php';
require_once plugin_dir_path( __FILE__ ) . 'short_codes.php';
require_once plugin_dir_path( __FILE__ ) . 'actions.php';

/**
 * Runs from main file
 */
function infopack_activation_action() { 

    infopack_add_bot_user();

    // setup first time and flush rewrite rules
    infopack_setup_custom_post_type(); 
    // Clear the permalinks after the post type has been registered.
    flush_rewrite_rules(); 

    // core/redirector.php
    infopack_create_redirect_table_hook();

    // TEMP, delete in future release
    infopack_delete_redirect_table_hook();
    infopack_delete_infopack_current_version_option();
}

add_filter('http_request_args', function ($r) {
  $r['timeout'] = 60;
  return $r;
});

add_action( 'init', 'infopack_setup_custom_post_type' );

add_action( 'wp_enqueue_scripts', 'infopack_enqueue_css_callback' );

function infopack_enqueue_css_callback() {
  wp_enqueue_style( 'dashicons' );
}
