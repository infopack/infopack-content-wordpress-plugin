<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_shortcode('infopack-meta', 'infopack_meta_shortcode');

function infopack_meta_shortcode($shortcode_atts = [], $content = null) {
    
    global $post; // Hämtar det aktuella inlägget eller sidan
    
    if (!$post) {
        return 'We have no meta for this page.';
    }
    
    $json = json_decode(get_post_meta($post->ID, 'meta', true));
    
    if (!$json) {
        return '';
    }
    
    if (isset($shortcode_atts['major_only'])) {
        $json->meta->version = explode(".", $json->meta->version)[0];
    }
    
    return infopack_meta_renderer($json);
}

add_shortcode('infopack-current-version', 'current_version_shortcode');

function current_version_shortcode() {
  $posts = get_posts(array(
    'post_type'      => 'infopack_collection',
    'posts_per_page' => 1,
    'meta_query'     => array(
        array(
            'key'     => 'active',
            'value'   => '1',
            'compare' => '='
        )
    )
  ));

  if (!empty($posts)) {
    $post_id = $posts[0]->ID;
    return get_post_meta($post_id, 'version', true);
  } else {
    return '';
  }

}

add_shortcode('infopack-url', 'infopack_shortcode_url_callback');

function infopack_shortcode_url_callback($shortcode_atts = []) {
  $extraction_results = infopack_utils_resolve_shortcode_atts($shortcode_atts);

  if(!$extraction_results[0]) {
    return $extraction_results[1];
  }

  $atts = $extraction_results[1];

  return infopack_utils_generate_url($atts);
}

add_shortcode('infopack-toc', 'infopack_toc');

function infopack_toc($shortcode_atts = []) {
  $tree = get_option( 'infopack_collection_expanded' );
  $tree = json_decode( $tree );

  $list = $tree->content;

  $filtered = filter_by_collection_path($list, $shortcode_atts["collection_path"]);

  $result = '<div class="infopack-toc"><ul>';
  foreach ($filtered as $item) {
    $collection_identifier = infopack_util_extract_collection_identifier($item->collectionPath);
    $x = explode('/', $item->filePath);
    array_pop( $x );
    $slug = infopack_build_path( array( $collection_identifier, $item->packagePath, implode('/', $x)) );
    $result .= '<li><a href="/infopack_content/' . $slug . '">' . $item->meta->title . '</a></li>';
  }
  $result .= '</ul></div>';

  return $result;
}

function filter_by_collection_path($list, $needle) {
  // Säkerställ att $needle är en sträng och inte null
  if (!is_string($needle) || empty($needle)) {
    return []; // Returnera en tom array om $needle är ogiltigt
  }
  return array_filter($list, function($item) use ($needle) {
    // Se till att $item är ett objekt eller array och att collectionPath finns
    $collectionPath = is_array($item) ? ($item['collectionPath'] ?? null) : ($item->collectionPath ?? null);
    
    // Kolla att collectionPath är en sträng och att den börjar med $needle
    return is_string($collectionPath) && strpos($collectionPath, $needle) === 0;
  });
}

add_shortcode('infopack-subpages', 'hierarchical_page_list_shortcode');

function hierarchical_page_list_shortcode($atts) {
  global $post;

  // Om vi inte är på en sida/inlägg, avbryt
  if (!isset($post)) {
      return '';
  }

  // Hämta post_type dynamiskt och kontrollera om det är hierarkiskt
  $post_type = get_post_type($post->ID);
  $post_type_object = get_post_type_object($post_type);

  // Om posttypen inte är hierarkisk, returnera tomt
  if (!$post_type_object || !$post_type_object->hierarchical) {
      return '';
  }

  return generate_hierarchical_list($post_type, $post->ID);
}

add_shortcode('infopack-dependencies', 'infopack_dependencies');

function infopack_dependencies($atts) {

  $posts = get_posts(array(
    'post_type'      => 'infopack_collection',
    'posts_per_page' => 1,
    'meta_query'     => array(
        array(
            'key'     => 'active',
            'value'   => '1',
            'compare' => '='
        )
    )
  ));

  if (!empty($posts)) {
    // Konvertera JSON till PHP-array
    $data = json_decode($posts[0]->post_content, true);

    // Kontrollera att `$meta['schema']` finns innan vi anropar funktionen
    if (isset($data['$meta']) && isset($data['$meta']['schema'])) {
        return infopack_renderer_dependency_tree($data['$meta']['schema']);
    } else {
        return "Fel: Ingen schema hittades.";
    }

  } else {
    return '';
  }
}

function generate_hierarchical_list($post_type, $parent_id) {
  $args = array(
      'post_type'   => $post_type,
      'post_status' => 'publish',
      'orderby'     => 'menu_order',
      'order'       => 'ASC',
      'post_parent' => $parent_id,
      'numberposts' => -1
  );

  $pages = get_posts($args);

  if (!$pages) {
      return ''; // Ingen lista om det inte finns barn
  }

  $output = '<ul>';
  foreach ($pages as $page) {
      $output .= '<li><a href="' . get_permalink($page->ID) . '">' . esc_html($page->post_title) . '</a>';

      $sub_list = generate_hierarchical_list($post_type, $page->ID);
      if ($sub_list) {
          $output .= $sub_list;
      }

      $output .= '</li>';
  }
  $output .= '</ul>';

  return $output;
}
