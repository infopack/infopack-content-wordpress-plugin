<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function infopack_meta_renderer($json) {
	$content = '<dl>';
	$content .= '<dt>Titel</dt><dd>' . $json->title . '</dd>';
	$content .= '<dt>Beskrivning</dt><dd>' . $json->description . '</dd>';
	$content .= '<dt>Paketnamn</dt><dd>' . $json->meta->name . '</dd>';
	$content .= '<dt>Version</dt><dd>' . $json->meta->version . '</dd>';
	$content .= '<dt>Sökväg</dt><dd>' . $json->path . '</dd>';
	if(property_exists($json, 'labels')) {
        foreach ($json->labels as $key => $value) {
            $content .= '<dt>' . $key . '</dt><dd>' . $value . '</dd>';
        }
	}
	$content .= '</dl>';
	
	return '<div class="infopack-content">' . $content . '</div>';
}

function infopack_content_renderer($irn, $html) {
    $storage_url = "https://storage.googleapis.com/storage.infopack.io/";
    $current_path = infopack_utils_generate_url($irn);
    
    $reg = '/src=(?:(?:"|\')?([^"|\'|\s]*))/m';
    $html = preg_replace_callback(
        $reg,
        function ($matches) use ($current_path) {
            
            $str = $matches[0];
            $linked_path = $matches[1];
            
            $arr = parse_url($linked_path);
            if(array_key_exists('host', $arr)) {
                return $str;
            } else {
                $new_path = infopack_path_join(dirname($current_path), $linked_path);
                return str_replace($linked_path, $new_path, $str);
            }
        },
        $html
    );
    return '<div class="infopack-content">' . $html . '</div>';
}

function infopack_renderer_dependency_tree($data) {
    if (!isset($data['content']) || !is_array($data['content'])) {
        return ''; // Om ingen content finns, returnera tomt
    }

    $html = '<ul>';
    
    foreach ($data['content'] as $item) {
        $title = isset($item['title']) ? esc_html($item['title']) : 'Ingen titel';
        $irn = isset($item['irn']) ? esc_html($item['irn']) : ''; // Hämta IRN om det finns

        // Lägg till title och IRN i listan
        $html .= '<li>' . $title;
        if (!empty($irn)) {
            $html .= ' <span style="color: gray;">(' . $irn . ')</span>';
        }

        // Om det finns subcontent, kör funktionen rekursivt
        if (isset($item['content']) && is_array($item['content'])) {
            $html .= infopack_renderer_dependency_tree($item); // Rekursivt anropa sig själv
        }
        
        $html .= '</li>';
    }
    
    $html .= '</ul>';
    
    return $html;
}