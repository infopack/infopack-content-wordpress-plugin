<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Load subcomponents
require_once plugin_dir_path( __FILE__ ) . 'infopack-collection-add-columns.php';
require_once plugin_dir_path( __FILE__ ) . 'infopack-content-add-columns.php';
require_once plugin_dir_path( __FILE__ ) . 'page-add-collection.php';
require_once plugin_dir_path( __FILE__ ) . 'page-settings.php';
