<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Remove new buttom in sub menu
 */
add_action('admin_menu', 'remove_add_new_button_for_cpt');

function remove_add_new_button_for_cpt() {
    global $submenu;
    if (isset($submenu['edit.php?post_type=infopack_collection'])) {
        unset($submenu['edit.php?post_type=infopack_collection'][10]); // Ta bort "Lägg till ny"
    }
}

add_action('admin_head', 'modify_existing_add_new_button');

function modify_existing_add_new_button() {
    $screen = get_current_screen();

    if ($screen->post_type === 'infopack_collection') {
        ?>
        <script>
            document.addEventListener("DOMContentLoaded", function() {
                let addNewButton = document.querySelector('.page-title-action'); // Hitta knappen
                if (addNewButton) {
                    addNewButton.href = "<?php echo admin_url('admin.php?page=create_infopack'); ?>"; // Byt URL
                }
            });
        </script>
        <?php
    }
}

add_action('admin_menu', 'register_custom_admin_page');

function register_custom_admin_page() {
    add_submenu_page(
        'edit.php?post_type=infopack_collection',
        'Läs in collection', 
        'Läs in collection', 
        'manage_options', 
        'create_infopack', 
        'render_custom_admin_page'
    );
}

function render_custom_admin_page() {
?>

<div class="wrap">
    <h1>Läs in infopack collection</h1>
    <form method="post" action="<?php echo admin_url('admin-post.php'); ?>">
        <?php wp_nonce_field('infopack_collection_action', 'infopack_collection_nonce'); ?>
        <input type="hidden" name="action" value="process_infopack_collection">
        <textarea name="infopack_collection_string" rows="10" style="width:100%;" placeholder="Klistra in JSON här..."></textarea>
        <p>
            <button type="submit" class="button button-primary">Läs in</button>
        </p>
    </form>
    <p>Läs mer och testa collections <a target="_blank" href="https://registry.infopack.io/collection">här</a>.</p>
</div>
<?php

}
