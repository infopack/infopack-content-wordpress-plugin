<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_filter('manage_infopack_collection_posts_columns', 'infopack_collection_custom_columns');

function infopack_collection_custom_columns($columns)
{
    return array_merge(
        $columns,
        array(
            'version' => __('Version'),
            'collection_identifier' => __('Collection Identifier'),
			'files' => __('Files'),
            'active' => __('Active')
        )
    );
}

add_action('manage_infopack_collection_posts_custom_column', 'infopack_collection_display_custom_columns', 10, 2);

function infopack_collection_display_custom_columns($column, $post_id)
{
    switch ($column) {
        case 'version':
			echo get_post_meta($post_id, 'version', true);
			break;
		case 'collection_identifier':
            echo get_post_meta($post_id, 'collection_identifier', true);
            break;
        case 'files':
            $collection_identifier = get_post_meta($post_id, 'collection_identifier', true);

            if ($collection_identifier) {
                // Hämta antal infopack_content med samma collection_identifier
                $args = array(
                    'post_type'   => 'infopack_content',
                    'post_status' => 'publish',
                    'meta_query'  => array(
                        array(
                            'key'   => 'collection_identifier',
                            'value' => $collection_identifier,
                            'compare' => '='
                        )
                    ),
                    'posts_per_page' => -1,
                    'fields' => 'ids'
                );

                $query = new WP_Query($args);
                $count = $query->found_posts;

                if ($count > 0) {
                    $link = admin_url('edit.php?post_type=infopack_content&collection_identifier=' . urlencode($collection_identifier));
                    echo '<a href="' . esc_url($link) . '" class="button button-link">' . $count . '</a>';
                } else {
                    echo '<a href="/wp-admin/admin-post.php?action=sync_collection_button&post_id=' . $post_id . '">Import</a>';
                }
            } else {
                echo 'Resync (0)';
            }
            break;
        case 'active':
            echo get_post_meta($post_id, 'active', true) ? 'Yes': '<a href="/wp-admin/admin-post.php?action=infopack_activate_collection&post_id=' . $post_id . '">No</a>';
            break;
    }
}

add_filter('manage_edit-infopack_collection_sortable_columns', function ($sortable_columns) {
    $sortable_columns['version'] = 'version';
    return $sortable_columns;
});

add_action('pre_get_posts', function ($query) {
    if (!is_admin() || !$query->is_main_query()) {
        return;
    }

    $orderby = $query->get('orderby');

    if ($orderby === 'version') {
        $query->set('meta_key', 'version'); // Ange vilket meta-fält som används för sortering
        $query->set('orderby', 'meta_value'); // Sortera på värdet
        $query->set('order', strtoupper($query->get('order')) === 'ASC' ? 'ASC' : 'DESC'); // Standard DESC
    }
});
