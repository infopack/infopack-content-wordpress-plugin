<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Add the Infopack settings page as a submenu under Settings.
 */
function infopack_options_page() {
    add_options_page(
        'Infopack Settings', // Page title
        'Infopack', // Menu title
        'manage_options', // Capability
        'infopack_info', // Menu slug
        'infopack_options_page_html' // Callback function
    );
}

/**
 * Register our infopack_options_page to the admin_menu action hook.
 */
add_action('admin_menu', 'infopack_options_page');


/**
 * Render migration submenu page
 */
function infopack_options_page_html() {
// check user capabilities
if ( ! current_user_can( 'manage_options' ) ) {
	return;
}

?>
<div class="wrap">
	<h4>Upgrade notice</h4>
	<p>If you just upgraded to 2.* version of this plugin. It is strongly recommended to delete old synced content since all content should be related to a collection.</p>
	<table>
		<thead>
			<tr>
				<th>Available shortcodes</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
		<tr>
			<td>[infopack-meta]</td>
			<td>Renders metadata for an infopack file.</td>
		</tr>
		<tr>
			<td>[infopack-url]</td>
			<td>Returns a valid url based on irn or package properties.</td>
		</tr>
		<tr>
			<td>[infopack-current-version]</td>
			<td>Renders the version of the active collection.</td>
		</tr>
		<tr>
			<td>[infopack-dependencies]</td>
			<td>Renders the dependencies of active collection.</td>
		</tr>
		</tbody>
	</table>
	<table>
		<thead>
			<tr>
				<th>Shortcode attributes</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>major_only</td>
				<td>Used in [infopack-meta] to only render major portion of version number.</td>
			</tr>
		</tbody>
	</table>
</div>
<?php
}