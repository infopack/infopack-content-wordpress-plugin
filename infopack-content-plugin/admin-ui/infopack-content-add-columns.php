<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_filter('manage_infopack_content_posts_columns', 'infopack_custom_columns');

function infopack_custom_columns($columns)
{
    return array_merge(
        $columns,
        array(
            'collection_identifier' => __('Collection Identifier'),
            'sync_identifier' => __('Sync Identifier'),
			'last_synced' => __('Last synced'),
			'resync' => __('Resync')
        )
    );
}

add_action('manage_infopack_content_posts_custom_column', 'infopack_display_custom_columns', 10, 2);
function infopack_display_custom_columns($column, $post_id)
{
    switch ($column) {
		case 'collection_identifier':
            echo get_post_meta($post_id, 'collection_identifier', true);
            break;
        case 'sync_identifier':
            echo get_post_meta($post_id, 'sync_identifier', true);
            break;
        case 'namespace':
            echo get_post_meta($post_id, 'namespace', true);
            break;
		case 'package_name':
				echo get_post_meta($post_id, 'package_name', true);
			break;
		case 'version':
			echo get_post_meta($post_id, 'version', true);
			break;
		case 'last_synced':
			echo get_post_meta($post_id, 'last_synced', true);
			break;
		case 'resync':
			echo '<a href="/wp-admin/admin-post.php?action=infopack_admin_sync&post_id=' . $post_id . '">Resync</a>';
			break;
    }
}


add_action('restrict_manage_posts', 'infopack_content_filter_by_collection_identifier');

function infopack_content_filter_by_collection_identifier() {
    global $typenow;

    // Kör endast om vi är i rätt post type
    if ($typenow == 'infopack_content') {
        $selected = isset($_GET['collection_identifier']) ? $_GET['collection_identifier'] : '';

        echo '<select name="collection_identifier">';
        echo '<option value="">Alla Collection Identifiers</option>'; // Standardval

        // Hämta alla unika värden från meta-fältet "collection_identifier"
        $identifiers = get_unique_meta_values('collection_identifier', 'infopack_content');

        foreach ($identifiers as $identifier) {
            echo '<option value="' . esc_attr($identifier) . '" ' . selected($selected, $identifier, false) . '>' . esc_html($identifier) . '</option>';
        }

        echo '</select>';
    }
}

add_action('pre_get_posts', 'infopack_content_filter_query_by_collection_identifier');

function infopack_content_filter_query_by_collection_identifier($query) {
    global $pagenow;

    if ($pagenow == 'edit.php' && isset($_GET['collection_identifier']) && !empty($_GET['collection_identifier']) && $query->is_main_query()) {
        $query->set('meta_query', array(
            array(
                'key'     => 'collection_identifier', // Meta-fältet vi filtrerar på
                'value'   => sanitize_text_field($_GET['collection_identifier']),
                'compare' => '='
            )
        ));
    }
}

function get_unique_meta_values($meta_key, $post_type) {
    global $wpdb;
    $query = $wpdb->prepare(
        "SELECT DISTINCT meta_value FROM {$wpdb->postmeta} 
        INNER JOIN {$wpdb->posts} ON {$wpdb->postmeta}.post_id = {$wpdb->posts}.ID
        WHERE meta_key = %s 
        AND post_type = %s 
        ORDER BY meta_value ASC",
        $meta_key,
        $post_type
    );
    
    return $wpdb->get_col($query);
}
