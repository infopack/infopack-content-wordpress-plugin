# Infopack content plugin

## Design goals for ver 2.*

* Ability to sync multiple collections
* Build infopacks with hierarchy
* Efficient route handeling
  * files named index will be considered the default content for its folder
  * files which are named exactly as its folder will be considered as an index file
